# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import pandas as pd
import plotly.graph_objs as go

# importation du fichier CSV de la population par tranche d'age et sexe
filename0 = 'evolution_population.csv'
filename1 = 'pop_par_sexe&age.csv'
filename2 = 'evolution_nbr_naissances_décès_soldenaturel.csv'
filename3 = 'evolution_nbr_naissances_femmesAgeProcréer_ICF.csv'
filename4 = 'pop_UE.csv'
filename5 = 'taux_fécondité_par_grp_ages.csv'

#'conversion' CSV en dataframe
evPop = pd.read_csv(filename0)
ds = pd.read_csv(filename1)
evSoldeNat = pd.read_csv(filename2)
evICF = pd.read_csv(filename3)
popUE = pd.read_csv(filename4)
txfecon = pd.read_csv(filename5)

#style css de la page
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div(children=[
    html.H1( # déclaration du titre en HTML
        'Bilan démographique',
        style={
            'textAlign': 'center',
            'color' : 'red',
            'text-decoration': 'underline'
        }# mise en page du titre
    ),

    html.P("Ce dashboard a pour but d'expliquer l'évolution de la population en France depuis 1995."
           " Pour ce faire, je me suis inspiré des données de l'INSEE et de leur explications.",
           style={
               'dislay' : 'block'
           }
           ),

    html.H2(
        "Evolution de la population",
        style={
            'color':'green'
        }
    ),

    html.P(
        "Au 1er janvier 2019, la France compte près de 67 millions d’habitants. Au cours de l’année 2018, la population a augmenté de 0,30 %."
    ),

    dcc.Graph(
        id='evPop',
        figure={
            'data': [
                {'x':evPop['année'], 'y':evPop['evolution population %'], 'type':'scatter','name':'Evolution de la population'},
            ],
            'layout':{
                'title':'Evolution de la population depuis 2008 en %',
                'mode':'lines+markers',
            }
        },
        style={'display':'under-block'}
    ),

    html.P(
        "Cependant nous pouvons observer que cette augmentation est une des pires que la France ait connu depuis un certain temps. Pourquoi ? C'est ce que nous allons "
        "expliquer danns la suite."
    ),
    html.P("Noton tout de même que la France est le 2ème pays le "
       "plus peuplé de l'Union Européenne derrière l'Allemagne."),

    dcc.Graph(
        id='popUE',
        figure={
            'data': [
                {'x': popUE['pays'], 'y':popUE['population'], 'type':'bar','name':'population'},
            ],
            'layout':{
                'title':'Classement des pays les plus peuplés de l\'UE'
            }
        }
    ),

    html.H2(
        'L\'évolution de la natalité et de la mortalité',
        style={
            'color' : 'green'
        }
    ),

    html.P(
        "Ici nous allons voir la cause principale de la diminution de l'augmentation de la population : la dimininution du solde naturel."
        "Le solde naturel par définition est la  différence entre les nombres de naissances et de décès."
        " Plus le solde est grand et plus l'évolution de la population est grande. "
        "Un solde naturel négatif est alarmant car il indique que la population est moins élevée que les années précédentes. "
    ),

    dcc.Dropdown(
        id='dropdown',
        options=[
            {'label': 'Evolution naissances et décès', 'value': 'naimort'},
            {'label': 'Evolution du solde naturel', 'value': 'solna'},
        ],
        value='naimort'
    ),

    dcc.Graph(
        id = "graph",
    ),

    html.P(
      'On peut voir ici que le solde naturel n\'a jamais été aussi bas ces '
      'dernières années (pour info la dernière fois c\'était à la fin de la 2nde Guerre Mondiale)'
      ' contrairement en 2006 où on avait atteint un nouveau pic grâce au mini baby-boom. '
      'Cette baisse peut s\'expliquer grâce à l\'augmentation des décès mais aussi en simultanée à la baisse des '
      'naissances. '
    ),
    html.P(
        'Mais pourquoi une telle baisse au niveau des natalités ? '
    ),

    html.H2(
        'Différentes raisons',
        style={
            'color' : 'green'
        }
    ),

    html.P(
        'Le graphe suivant se base sur les années 1995. Il est question ici '
        'du nombre de femmes ayant entre 15 et 50 ans, celles entre 20 et 40 ans et le nombre de naissances. '
        'Pour obtenir ce graphe il suffit de faire le rapport entre un des critères étudiés et ce même critère dans les années 1995. '
        'Le résultat est multiplié ensuite par 100 ce qui explique la valeur 100 dans tous les critères pour les années 1995. '
        'Une valeur supérieur signifie une augmentation du critère étudié tandis qu\'une valeur inférieure '
        'à 100 indique une diminution. '
    ),

    dcc.Graph(
        id = "femmesnais",
        figure={
            'data':[
                {'x':evICF['année'], 'y':evICF['nbr de femmes 15-50ans ind 100 en 1995'], 'type':'scatter','name':'nbr de femmes 15-50ans'},
                {'x':evICF['année'], 'y':evICF['nbr de femmes 20-40ans ind 100 en 1995'], 'type':'scatter','name':'nbr de femmes 20-40ans'},
                {'x':evICF['année'], 'y':evICF['nbr de naissances ind 100 en 1995'], 'type':'scatter','name':'nombre de naissances'},
            ],
            'layout':{
                'title':"Evolution du nombre de naissances et du nombre de femmes selon leur âge"
            }
        }
    ),

    html.P(
        'On note ici une diminution des naissances depuis 4ans déjà mais cette diminution tend '
        'à s\'atténuer avec 6 000 naissances de plus en 2017 par rapport à 2015. '
        'Cette diminution s\'explique par la baisse des femmes agées de 20 à 40ans soit '
        'l\'intervalle d\'âge où la fécondité est plus forte. '
    ),

    html.P(
        'Mais il n\'y pas que cela, en réalité le plus gros facteur reste une fécondité moindre '
        'comparé aux années précédentes.'
    ),

dcc.Graph(
        id = "txfecon",
        figure={
            'data':[
                {'x':txfecon['année'], 'y':txfecon['15-24ans'], 'type':'scatter','name':'15-24ans'},
                {'x':txfecon['année'], 'y':txfecon['25-29ans'], 'type':'scatter','name':'25-29ans'},
                {'x':txfecon['année'], 'y':txfecon['30-34ans'], 'type':'scatter','name':'30-34ans'},
                {'x':txfecon['année'], 'y':txfecon['35-39ans'], 'type':'scatter','name':'35-39ans'},
                {'x':txfecon['année'], 'y':txfecon['40-50ans'], 'type':'scatter','name':'40-50ans'}
            ],
            'layout':{
                'title':"Taux de fécondité selon l'âge de la femme"
            }
        }
    ),

    html.P(
        'Comment lire ce graphique ? Pour 100 femmes en moyenne agés de 30 à 34ans en 2013,par exemple, '
        'on a en moyenne 12,4 enfants (pas ensemble bien évidemment). '
        'On constate une nette diminution pour les femmes âgées de 25 à 29ans et de 15 à 24ans qui se fait depuis les années 2 000 contrairement à celles âgées de '
        '30 à 34ans où l\'on assiste à une augmentation du nombre d\'enfants moyen pour 100 femmes.'
        ' Cela peut s\'expliquer par le fait que l\'âge moyen à la maternité croît régulièrement '
        ': aujourd\'hui elle est de 30.6 ans contre 29.7 ans il y a dix ans de cela. '
        'Cela entraîne le fait que l\'ICF diminue lui aussi, voir figure suivante :  '

    ),

    dcc.Dropdown(
        id='my-dropdown',
        options=[
            {'label': 'IFC en France', 'value': 'ICF_F'},
            {'label': 'IFC en UE', 'value': 'ICF_UE'},
        ],
        value='ICF_F'
    ),

    dcc.Graph(
        id='graph_change',
    ),

    html.P(
    'L\'IFC, en bref, est un indicateur qui donne le nombre moyen d\'enfants '
    'par femme. Ici pas de surprise donc, on observe une diminution. '
    'Néanmoins, la France reste le pays de l\'Union Européenne où la fécondité est la plus grande.'
    ),


    html.H2(
        "Conclusion",
        style={
            'color' : 'green'
        }
    ),

    html.P(
        'La population de la France connaît une diminution de son évolution mais la situation est loin d\'être '
        'alarmante car la population ne cesse de croître chaque année. Ici nous n\'avons pas pris en compte le solde '
        'migratoire car sa valeur est faible devant le solde naturel mais il est bon de '
        'savoir que celui-ci est positif (on ne fuit pas le pays, bien au contraire.)'
    )

])


@app.callback(
    dash.dependencies.Output('graph_change', 'figure'),
    [dash.dependencies.Input('my-dropdown', 'value')])
def update_output(value):
    y_array_dict = {
        'ICF_F': txfecon['ICF'],
        'ICF_UE': [1.89,1.78,1.77,1.75,1.74,1.71,1.69,1.69,1.65],
    }
    if(value == 'ICF_F'):
        return {
            'data': [{
                'type': 'bar',
                'x': txfecon['année'],
                'y': y_array_dict[value]
            }],
            'layout': {
                'title': 'IFC en France selon les années'
            }
        }
    else:
        return {
            'data': [{
                'type': 'bar',
                'x':['France','Suède','Irlande','Danemark','Angleterre','Roumanie','Tchèque','Lettonie','Belgique'],
                'y':[1.89,1.78,1.77,1.75,1.74,1.71,1.69,1.69,1.65]
            }],
            'layout': {
                'title': 'IFC en Union Européenne'
            }
        }

@app.callback(
    dash.dependencies.Output('graph', 'figure'),
    [dash.dependencies.Input('dropdown', 'value')])
def update_output(value):
    y_array_dict = {
        'naimor': evSoldeNat.loc[:,['naissances','décès']],
        'solna': evSoldeNat['solde naturel'],
    }
    if(value == 'solna'):
        return {
            'data':[
                {'x':evSoldeNat['année'], 'y':evSoldeNat['solde naturel'], 'type':'bar','name':'solde naturel'}
            ],
            'layout':{
                'title':"Evolution du solde naturel"
            }
        }
    else:
        return {
            'data': [
                {'x': evSoldeNat['année'], 'y': evSoldeNat['naissances'], 'type': 'Scatter', 'name': 'naissances'},
                {'x': evSoldeNat['année'], 'y': evSoldeNat['décès'], 'type': 'scatter', 'name': 'décès'},
            ],
            'layout': {
                'title': "Evolution des naissances et des décès",
            }
        }



if __name__ == '__main__':
    app.run_server(debug=False)